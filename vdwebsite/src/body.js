import React, {Component} from 'react';
import {Image} from 'react-bootstrap';
import avatar from './img/avatar.jpg';
import './general.css';
export default class Body extends Component{
    render(){
        return(
            <div className={"bodycss text-center bodycontainer"}>
                <Image src={avatar} circle width={"7.5%"}/>
                <h1>HI, I AM VU DO</h1>
                <h2>Computer Science & Mathematics at Mount Saint Mary's University</h2>
            </div>
        )
    }
}