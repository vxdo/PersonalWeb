import React, { Component } from 'react';
import './general.css';
import {Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink,} from 'reactstrap';
import {Image} from 'react-bootstrap';
import vdicon from './img/vdicon.png';

export default class Navigator extends Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false
        };
    }
    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }
    render() {
        return (
            <div className="Navi navcontainer">
                <Navbar light expand="md">
                    <NavbarBrand>
                        <Image src={vdicon} circle width={"35%"} ></Image>
                    </NavbarBrand>
                    <NavbarToggler onClick={this.toggle} />
                    <Collapse isOpen={this.state.isOpen} navbar>
                        <Nav className="ml-auto " navbar>
                            <NavItem>
                                <NavLink href="./index.js">Home</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="https://github.com/reactstrap/reactstrap">About</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="/">Resume</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="/">Projects</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="/">Stuffs</NavLink>
                            </NavItem>
                        </Nav>
                    </Collapse>
                </Navbar>
            </div>
        );
    }
}
