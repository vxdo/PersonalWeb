import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Navigator from './navbar';
import Body from './body';
import registerServiceWorker from './registerServiceWorker';
import 'bootstrap/dist/css/bootstrap.min.css';

ReactDOM.render(<Navigator />, document.getElementById('navi'));
ReactDOM.render(<Body />, document.getElementById('body'))
registerServiceWorker();
